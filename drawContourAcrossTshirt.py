import cv2
import warnings
warnings.filterwarnings("ignore")

# Download image from this link: https://i.postimg.cc/HjDVQ4R8/segmentation.png

image = cv2.imread("./segmentation.png") # check the image is in .png or .jpg after downloading from the link
image = cv2.GaussianBlur(image, (7,7), 0)
color_map = cv2.applyColorMap(image, cv2.COLORMAP_JET)
result_image = image.copy()
gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

contours, _ = cv2.findContours(gray_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
max_area = 0
max_contour = None
for contour in contours:
    area = cv2.contourArea(contour)
    if area > max_area:
        max_area = area
        max_contour = contour
        
alpha = 0.005 # decrease the value of alpha to generate more vertices across the segmentatiopn
epsilon = alpha*cv2.arcLength(max_contour,True)
approx_corners = cv2.approxPolyDP(max_contour,epsilon,True)
for i in range(len(approx_corners)):
    cv2.line(result_image, tuple(approx_corners[i][0]), tuple(approx_corners[(i+1)%len(approx_corners)][0]), (0,255,0), 3)
    print(tuple(approx_corners[i][0]))

cv2.imshow("SegmentationContour.png", result_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
